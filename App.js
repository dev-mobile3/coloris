// import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { StatusBar } from "expo-status-bar";
import HomePage from "./components/HomePage";
import HistoryScreen from './components/HistoryScreen';
import ImageScreen from "./components/ImageScreen";
import ComparisonScreen from "./components/ComparisonScreen";
import { ListContext } from './Contexts';


const Stack = createNativeStackNavigator();

export default function App() {

  {/*Test de structure de données : on modélise une comparaison par :
  - 2 couleurs  
  - pourcentage de ressemblance
  - nom
  éventuellement : thumbnail de l'image d'origine ?
  */}

  const list = [
    {
      key: 0,
      name: "comparaison 1",
      colors: ['#fff', '#000'],
      match: 0.1
    },
    {
      key: 1,
      name: "papier peint",
      colors: ['#abc', '#cba'],
      match: 0.5
    },
    {
      key: 2,
      name: "t-shirt",
      colors: ['#b32', '#c45'],
      match: 0.82
    },
    {
      key: 3,
      name: "vase",
      colors: ['#2d3', '#4e6'],
      match: 0.73
    },
    {
      key: 4,
      name: "sac",
      colors: ['#46f', '#48d'],
      match: 0.62
    }
  ];

  const testFunc = () => {
    list.push({
      key: list.length,
      name: "test push",
      colors: ['#0f0', '#00f'],
      match: 0.01
    });
  };

  const updateName = (key, name) => {
    list[key] = name;
  };
  
  return (
    <NavigationContainer>
      <ListContext.Provider value={{list, testFunc, updateName}}>
        <StatusBar style="light" />
        <Stack.Navigator
          initialRouteName="Home"
          screenOptions={{ headerShown: false }}
        >
          <Stack.Screen name="Home" component={HomePage} />
          <Stack.Screen name="ImageScreen" component={ImageScreen} />
          <Stack.Screen name="ComparisonScreen" component={ComparisonScreen} />
          <Stack.Screen name="HistoryScreen" component={HistoryScreen}/>
        </Stack.Navigator>
      </ListContext.Provider>
    </NavigationContainer>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

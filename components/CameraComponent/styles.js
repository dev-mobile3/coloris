import { StyleSheet, Dimensions } from "react-native";

const styles = StyleSheet.create({
  camera: {
    width: "95%",
    aspectRatio: 1,
    resizeMode: "cover",
  },
  buttonContainer: {
    backgroundColor: "transparent",
    flexDirection: "row",
    justifyContent: "center",
    margin: 20,
    bottom: 0,
    position: "absolute",
  },
  flipButton: {
    flex: 1,
  },
  cameraButton: {
    position: "absolute",
    right: 0,
  },
  logo: {
    width: 50,
    height: 50,
    resizeMode: "contain",
  },
  image: {
    width: "95%",
    aspectRatio: 1,
    borderRadius: (Dimensions.get("window").width * 0.95) / 2,
    borderWidth: 2,
    borderColor: "#ffffff",
  },
});

export default styles;

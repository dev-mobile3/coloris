import React, { useState, useEffect, useRef, useContext } from "react";
import { View, TouchableOpacity, Image } from "react-native";
import { Camera } from "expo-camera";
import * as MediaLibrary from "expo-media-library";
import { manipulateAsync, FlipType, SaveFormat } from "expo-image-manipulator";
import styles from "./styles";
import CustomText from "../CustomText";
import { ImageContext, ComparisonContext } from "../../Contexts";

const CameraComponent = (props) => {
  const { context } = props;

  const ctx = (context === "primary") ? ImageContext : ComparisonContext;

  const [hasCameraPermission, setHasCameraPermission] = useState(null);
  const [hasMediaLibraryPermission, setHasMediaLibraryPermission] =
    useState(null);
  const [type, setType] = useState(Camera.Constants.Type.back);
  const [image, setImage] = useContext(ctx);
  const ref = useRef(null);

  const flipImage = async (data) => {
    const manip = await manipulateAsync(
      data.uri,
      [{ rotate: 180 }, { flip: FlipType.Vertical }],
      { compress: 1, format: SaveFormat.PNG }
    );
    await setImage(manip);
  };

  const takePhotoAsync = async () => {
    const data = await ref.current.takePictureAsync();
    console.log("🚀 ~ file: index.js ~ line 18 ~ takePhotoAsync ~ data", data);
    type === Camera.Constants.Type.front ? flipImage(data) : setImage(data);
  };

  const flipCamera = () => {
    setType(
      type === Camera.Constants.Type.back
        ? Camera.Constants.Type.front
        : Camera.Constants.Type.back
    );
  };

  useEffect(() => {
    async function askPermissions() {
      const cameraPermission = await Camera.requestCameraPermissionsAsync();
      const mediaLibraryPermission =
        await MediaLibrary.requestPermissionsAsync();
      setHasCameraPermission(cameraPermission.status === "granted");
      setHasMediaLibraryPermission(mediaLibraryPermission.status === "granted");
    }
    askPermissions();
  }, []);

  if (hasCameraPermission === null) {
    return <View />;
  }
  if (hasCameraPermission === false) {
    return <CustomText content="No access to camera" />;
  }
  return image ? (
    <Image source={{ uri: image.uri }} style={styles.image} />
  ) : (
    <Camera style={styles.camera} type={type} ratio="1:1" ref={ref}>
      <View style={styles.buttonContainer}>
        <TouchableOpacity style={styles.flipButton} onPress={flipCamera}>
          <Image
            source={require("../../assets/images/flip.png")}
            style={styles.logo}
          />
        </TouchableOpacity>
        <TouchableOpacity style={styles.cameraButton} onPress={takePhotoAsync}>
          <Image
            source={require("../../assets/images/camera.png")}
            style={styles.logo}
          />
        </TouchableOpacity>
      </View>
    </Camera>
  );
};

export default CameraComponent;

import React, { useState, useEffect, useCallback } from "react";
import { View, ImageBackground, Image } from "react-native";
import { Lora_400Regular } from "@expo-google-fonts/lora";
import * as Font from 'expo-font';
import * as SplashScreen from 'expo-splash-screen';
import StyledButton from "../StyledButton";
import styles from "./styles";
import CustomText from "../CustomText";
import { ListContext } from "../../Contexts";
import WeatherCard from "../WeatherCard";

const HomePage = ({ route, navigation }) => {

  const [appIsReady, setAppIsReady] = useState(false);

  useEffect(() => {
    async function prepare() {
      try {
        await SplashScreen.preventAutoHideAsync();
        await Font.loadAsync({ Lora_400Regular });
      } catch (e) {
        console.warn(e);
      } finally {
        // Tell the application to render
        setAppIsReady(true);
      }
    }

    prepare();
  }, []);

  const onLayoutRootView = useCallback(async () => {
    if (appIsReady) {
      await SplashScreen.hideAsync();
    }
  }, [appIsReady]);

  const { list, testFunc } = React.useContext(ListContext);

  if (!appIsReady) {
    return null;
  } else {
    return (
      <View style={styles.container} onLayout={onLayoutRootView}>
        <ImageBackground
          source={require("../../assets/images/home.jpg")}
          style={styles.image}
        />

        <View style={styles.titles}>
        <Image source={require("../../assets/images/colorisIcon.png")} style={styles.icon} />
          <CustomText content="Coloris" style={styles.title} />
          <WeatherCard/>
        </View>

        

        <View style={styles.buttonsContainer}>
          <StyledButton
            type="primary"
            content={"Take Photo"}
            onPress={() => {
              navigation.navigate("ImageScreen", {type: "camera", step: 1});
            }}
          />

          <StyledButton
            type="secondary"
            content={"Import Photo"}
            onPress={() => {
              navigation.navigate("ImageScreen", {type: "gallery", step: 1});
            }}
          />

          <StyledButton
            type="primary"
            content={"History"}
            onPress={() => {
              navigation.navigate("HistoryScreen", list)
            }}
          />

          {/*<StyledButton
            type="secondary"
            content={"Add Element"}
            onPress={() => {
              testFunc();
              console.log("LISTE : " + list);
            }}
          />*/}

        </View>
      </View>
    );
  }
};

export default HomePage;

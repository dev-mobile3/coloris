import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  titles: {
    flex: 1,
    marginTop: "20%",
    width: "100%",
    alignItems: "center"
  },
  title: {
    fontSize: 55,
    fontWeight: "600",
    marginBottom: "10%"
  },
  image: {
    width: "100%",
    height: "100%",
    resizeMode: "cover",
    position: "absolute",
  },
  buttonsContainer: {
    flex: 4,
    position: "absolute",
    bottom: 80,
    width: "100%",
  },
  icon: {
    width: 80,
    height: 80,
    resizeMode: "contain",
  },
});

export default styles;

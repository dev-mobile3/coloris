import { View, Text } from "react-native";
import React from "react";

const CustomText = (props) => {
  const { content, style } = props;

  return (
    <View>
      <Text style={[style, { fontFamily: "Lora_400Regular" }]}>{content}</Text>
    </View>
  );
};

export default CustomText;

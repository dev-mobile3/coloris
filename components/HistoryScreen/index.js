import { StyleSheet, Text, View, FlatList, Dimensions, ImageBackground } from 'react-native'
import React from 'react'
import { backgroundColor } from 'react-native/Libraries/Components/View/ReactNativeStyleAttributes'
import styles from './styles'
import HistoryElement from '../HistoryElement'
import ColorCircle from '../ColorCircle'
import { render } from 'react-dom'
import { ListContext } from '../../Contexts'
import CustomText from '../CustomText'

const HistoryScreen = (props) => {

  //const { list } = props;
  const { list, testFunc } = React.useContext(ListContext);

  const renderElement = ({ item }) => (
    <HistoryElement comparison={item}/>
  );
  
  return (
    <View style={styles.screenContainer}>

      <ImageBackground
        source={require("../../assets/images/home.jpg")}
        style={styles.image}
      />
      

      <View style={styles.titles}>
        <CustomText content="Historique" style={styles.title} />
      </View>
      
      <View style={{ flex: 5, alignItems: 'center', backgroundColor: "#666"}}>
        <FlatList data={list} renderItem={renderElement}/>
      </View>

    </View>
  );
  
};

export default HistoryScreen
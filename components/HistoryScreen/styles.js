import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  
  title: {
    //marginTop: '40%',
    //width: '100%',
    //alignItems: 'center',
    fontSize: 45,
    fontWeight: '600'
  },
  screenContainer: {
    height: '100%'
  },
  image: {
    width: "100%",
    height: "100%",
    resizeMode: "cover",
    position: "absolute",
  },
  titles: {
    flex: 1,
    width: "100%",
    alignItems: "center",
    justifyContent: 'center',
    backgroundColor: "#aa3c39"
  },
  title: {
    fontSize: 45,
    fontWeight: "600"
  }

});

export default styles;
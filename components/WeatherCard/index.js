import { View, Text } from 'react-native'
import React, { useState, useEffect } from 'react'
import weatherCodes from './weatherCodes';
import styles from './styles';




const WeatherCard = () => {

  const [date, setDate] = useState();
  const [sun, setSun] = useState({sunrise: "", sunset: ""});
  const [weather, setWeather] = useState();
  const [temps, setTemps] = useState({min: 0, max: 0});

  const getWeather = async () => {
    const response = await fetch('https://api.open-meteo.com/v1/forecast?latitude=50.357&longitude=3.518&daily=weathercode,temperature_2m_max,temperature_2m_min,sunrise,sunset&timezone=Europe%2FBerlin');
    const json = await response.json();
    setDate(json.daily.time[0]);
    setSun({
      sunrise: json.daily.sunrise[0].split('T')[1],
      sunset: json.daily.sunset[0].split('T')[1]
    });
    setWeather(weatherCodes[json.daily.weathercode[0]]);
    setTemps({
      min: json.daily.temperature_2m_min[0],
      max: json.daily.temperature_2m_max[0]
    });
    console.log("getWeather");
  }

  useEffect(() => {getWeather()}, []);
  //getWeather();

  return (

    <View style={styles.card}>
      <Text style={styles.title}>Weather in Valenciennes</Text>

      <View style={{flexDirection: 'row'}}>
        <View style={styles.column}>
          <Text style={styles.left}>Weather :   </Text>
          <Text style={styles.left}>Sun :   </Text>
          <Text style={styles.left}>Temperatures :   </Text>
        </View>
          
        <View style={styles.column}>
          <Text style={styles.right}>{weather}</Text>
          <Text style={styles.right}>{sun.sunrise + " - " + sun.sunset}</Text>
          <Text style={styles.right}>{temps.min + "°C - " + temps.max + "°C"}</Text>
        </View>
      </View>

      <Text style={{fontStyle: 'italic'}}>Data from open-meteo</Text>
    </View>

  );

};

export default WeatherCard
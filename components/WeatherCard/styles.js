import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  card: {
    width: "80%", 
    backgroundColor: "#99ab", 
    alignSelf: 'center', 
    alignItems: 'center',
    borderWidth: 3,
    borderRadius: 25,
    flexDirection: "column",

  },
  column: {
    flexDirection: "column",
    alignItems: "center"
  },
  left: {
    fontWeight: "bold",
    alignSelf: "flex-end"
  },
  right: {
    alignSelf: "flex-start"
  },
  title: {
    fontSize: 20,
    fontWeight: "bold"
  },
  date: {
    fontSize: 15,
    fontStyle: "italic"
  }
});

export default styles;

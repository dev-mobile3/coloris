import { View, Text, StyleSheet, Image } from 'react-native'
import React from 'react'

const ColorCircle = (props) => {

  const { color, image } = props;

  return (
    <>
      <View style={{...styles.circle}}>
        <Image style={styles.image} source={require('../../assets/images/home.jpg')} />
      </View>
      <View>
        <View style={{...styles.colorCircle, backgroundColor: color}}/>
      </View>
    </>
  );

};

{/*borderRadius : dépend de la taille du component.
Solution : mettre height en prop ? mettre un borderRadius très grand pour toujours faire un cercle ? */}

const styles = StyleSheet.create({
  circle: {
    height: '75%',
    aspectRatio: 1,
    borderRadius: 100,
    borderWidth: 2
  },
  colorCircle: {
    height: '50%',
    aspectRatio: 1,
    borderRadius: 100,
    position: 'absolute', 
    zIndex: 2,
    alignSelf: 'flex-end',
    top: -5
  },
  image: {
    width: "100%",
    height: "100%",
    resizeMode: "cover",
    position: "absolute",
    borderRadius: 100,
  }
});

export default ColorCircle;
import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    top: 10,
    width: "100%",
    padding: 15,
  },
  button: {
    height: 80,
    borderRadius: 40,
    justifyContent: "center",
    alignItems: "center",
  },
  text: {
    fontSize: 25,
    fontWeight: "500",
  },
});

export default styles;

import React from "react";
import { View, Pressable } from "react-native";
import styles from "./styles";
import CustomText from "../CustomText";

const StyledButton = (props) => {
  const { type, content, onPress } = props;

  const backgroundColor = type === "primary" ? "#171A20CC" : "#FFFFFFA6";
  const textColor = type === "primary" ? "#FFFFFF" : "#171A20";

  return (
    <View style={styles.container}>
      <Pressable
        style={[{ backgroundColor: backgroundColor }, styles.button]}
        onPress={() => onPress()}
      >
        <CustomText
          content={content}
          style={[styles.text, { color: textColor }]}
        />
      </Pressable>
    </View>
  );
};

export default StyledButton;

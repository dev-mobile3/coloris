import { Image } from "react-native";
import React, { useEffect, useContext } from "react";
import * as ImagePicker from "expo-image-picker";
import { useNavigation } from "@react-navigation/native";
import styles from "./styles";
import { ImageContext } from "../../Contexts";

const ImagePickerComponent = () => {
  const navigation = useNavigation();

  const [image, setImage] = useContext(ImageContext);

  useEffect(() => {
    async function pickImage() {
      let data = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        allowsEditing: true,
        aspect: [1, 1],
        quality: 1,
      });
      console.log("🚀 ~ file: index.js ~ line 20 ~ pickImage ~ data", data);
  
      if (!data.cancelled) {
        setImage(data);
      } else {
        navigation.navigate("Home");
      }
    }
    pickImage()
  }, [])
  

  return image && <Image source={{ uri: image.uri }} style={styles.image} />;
};

export default ImagePickerComponent;

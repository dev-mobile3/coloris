import { StyleSheet, Dimensions } from "react-native";

const styles = StyleSheet.create({
  image: {
    width: "95%",
    aspectRatio: 1,
    borderRadius: (Dimensions.get("window").width * 0.95) / 2,
    borderWidth: 2,
    borderColor: "#ffffff",
  },
});

export default styles;

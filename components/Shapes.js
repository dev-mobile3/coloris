import { View, StyleSheet } from "react-native";
import React from "react";

export const Circle = (props) => {
  let { size, Xcoord, Ycoord } = props;
  return (
    <View
      style={[
        styles.circle,
        {
          top: Ycoord - size,
          left: Xcoord - size / 2,
          width: size,
          height: size,
          borderRadius: size / 2,
        },
      ]}
    />
  );
};

const styles = StyleSheet.create({
  circle: {
    backgroundColor: "transparent",
    borderWidth: 3,
    borderColor: "#fcfcfc",
    position: "absolute",
  }
});

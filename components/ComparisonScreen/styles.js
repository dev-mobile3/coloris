import { Dimensions, StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
  },
  image: {
    flex: 1,
    marginTop: 40,
    marginBottom: 40,
    width: "100%",
    aspectRatio: 1,
    borderRadius: (Dimensions.get("window").width) / 2,
    borderWidth: 2,
    borderColor: "#ffffff",
  },
  backgroundImage: {
    width: "100%",
    height: "100%",
    resizeMode: "cover",
    position: "absolute",
  },
  bigText: {
    fontSize: 20,
    fontWeight: "bold"
  },
});

export default styles;

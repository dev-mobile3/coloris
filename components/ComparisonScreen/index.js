import { View, ImageBackground } from "react-native";
import React, { useEffect, useState, useContext } from "react";
import * as FileSystem from "expo-file-system";
import Canvas, { Image as CanvasImage } from "react-native-canvas";
import styles from "./styles";
import CustomText from "../CustomText";
import { ListContext } from "../../Contexts";

const ComparisonScreen = ({ route, navigation }) => {
  const { image, comparison } = route.params;
  const [imageData, setImageData] = useState(null);
  const [comparisonData, setComparisonData] = useState(null);
  const [imageCanvas, setImageCanvas] = useState(null);
  const [comparisonCanvas, setComparisonCanvas] = useState(null);

  const { list } = React.useContext(ListContext);
  let firstColor;
  let secondColor;
  let compare;

  useEffect(() => {
    if (imageCanvas && comparisonCanvas) {
      async function readImage(img, cvs, set) {
        let base64 = await FileSystem.readAsStringAsync(img.uri, {
          encoding: FileSystem.EncodingType.Base64,
          compress: 0.4,
        });
        const src = "data:image/jpeg;base64," + base64;

        const photo = new CanvasImage(cvs);
        photo.src = src;

        cvs.width = image.width;
        cvs.height = image.height;

        const context = cvs.getContext("2d");

        photo.addEventListener("load", () => {
          context.drawImage(photo, 0, 0);
          context.getImageData(0, 0, cvs.width, cvs.height).then((data) => {
            set(data);
          });
        });
      }
      !imageData && readImage(image, imageCanvas, setImageData);
      !comparisonData &&
        readImage(comparison, comparisonCanvas, setComparisonData);
      if (imageData) {
        console.log(
          "🚀 ~ file: index.js ~ line 47 ~ useEffect ~ imageData",
          imageData
        );
        firstColor = averageColors(imageData);
        console.log(
          "🚀 ~ file: index.js ~ line 43 ~ useEffect ~ firstColor",
          firstColor
        );
      }
      if (comparisonData) {
        console.log(
          "🚀 ~ file: index.js ~ line 60 ~ useEffect ~ comparisonData",
          comparisonData
        );
        secondColor = averageColors(comparisonData);
        console.log(
          "🚀 ~ file: index.js ~ line 45 ~ useEffect ~ secondColor",
          secondColor
        );
        compare = compareColors(firstColor, secondColor);
        console.log(
          "🚀 ~ file: index.js ~ line 59 ~ useEffect ~ compare",
          compare
        );
      }
      list.push({
        key: list.length,
        name: "Nouvelle comparaison",
        colors: [firstColor, secondColor],
        match: compare,
      });
      console.log("Push fait sur list");
    }
  });

  {
    /*Computes match between 2 rgb colors, from 0 (no match) to 1 (match). */
  }
  const compareColors = (c1, c2) => {
    let diffR = (c2.r / 255 - c1.r / 255) ** 2;
    let diffG = (c2.g / 255 - c1.g / 255) ** 2;
    let diffB = (c2.b / 255 - c1.b / 255) ** 2;
    let distance = Math.sqrt(diffR + diffG + diffB);
    //max : sqrt(3)
    return (Math.sqrt(3) - distance) / Math.sqrt(3);
  };

  const averageColors = (photoData) => {
    let r = 0;
    let g = 0;
    let b = 0;
    const data = photoData.data;
    const len = photoData.width * photoData.height;
    for (let i = 0; i < len; i++) {
      r += data[i + 0] * data[i + 0];
      g += data[i + 1] * data[i + 1];
      b += data[i + 2] * data[i + 2];
    }
    r = Math.sqrt(r / len);
    g = Math.sqrt(g / len);
    b = Math.sqrt(b / len);
    return { r: r, g: g, b: b };
  };

  const renderMatch = () => {
    return (
      firstColor && (
        <View style={{flex: 0.1}}>
          <CustomText
            style={styles.bigText}
            content={`Match ${compare * 100} %}`}
          />
        </View>
      )
    );
  };

  return (
    <View style={styles.container}>
      <ImageBackground
        source={require("../../assets/images/home.jpg")}
        style={styles.backgroundImage}
      />
      <Canvas
        ref={(canvas) => {
          setImageCanvas(canvas);
        }}
        style={styles.image}
      />
      {renderMatch()}
      <Canvas
        ref={(canvas) => {
          setComparisonCanvas(canvas);
        }}
        style={styles.image}
      />
    </View>
  );
};

export default ComparisonScreen;

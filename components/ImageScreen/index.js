import { View, ImageBackground, Dimensions, Alert } from "react-native";
import React, { useState } from "react";
import { manipulateAsync, SaveFormat } from "expo-image-manipulator";
import CameraComponent from "../CameraComponent";
import ImagePickerComponent from "../ImagePickerComponent";
import { Circle } from "../Shapes";
import { ImageContext, ComparisonContext } from "../../Contexts";
import StyledButton from "../StyledButton";
import CustomText from "../CustomText";
import styles from "./styles";

const ImageScreen = ({ route, navigation }) => {
  const { type, step } = route.params;

  const [image, setImage] = useState(null);
  const [comparison, setComparison] = useState(null);
  const [circle, setCircle] = useState(null);
  const [isCropped, setIsCropped] = useState(false);

  const options =
    step === 1
      ? { get: image, set: setImage, context: ImageContext }
      : { get: comparison, set: setComparison, context: ComparisonContext };

  let cropImage = async () => {
    if (circle) {
      setIsCropped(true);
      let startX = circle.Xcoord - circle.size / 2;
      let startY = circle.Ycoord - circle.size;

      let viewSize = Dimensions.get("window").width * 0.95;
      let imageSize = options.get.width;
      let cornerX = (startX * imageSize) / viewSize - 50;
      let cornerY = (startY * imageSize) / viewSize;
      let cropSize = (circle.size * imageSize) / viewSize;
      const manipResult = await manipulateAsync(
        options.get.uri,
        [
          {
            crop: {
              height: cropSize,
              originX: cornerX,
              originY: cornerY,
              width: cropSize,
            },
          },
        ],
        { compress: 0, format: SaveFormat.PNG }
      );
      options.set(manipResult);
      setCircle(null);
    } else {
      Alert.alert("Action Required", "Select a portion of the image.");
    }
  };

  let placecircle = (evt) => {
    console.log("🚀 ~ file: index.js ~ line 32 ~ placecircle ~ evt", evt);
    setCircle({ size: 120, Xcoord: evt.pageX, Ycoord: evt.pageY });
  };

  let restart = () => {
    options.set(null);
    setIsCropped(false);
    if (type === "gallery") {
      navigation.navigate("Home");
    }
  };

  let confirm = () => {
    if (options.get) {
      // MediaLibrary.saveToLibraryAsync(image.uri);
      setIsCropped(false);
      step == 1
        ? navigation.navigate("ImageScreen", { type: "camera", step: 2 })
        : navigation.navigate("ComparisonScreen", {image: image, comparison: comparison});
    }
  };

  return (
    <View style={styles.container}>
      <ImageBackground
        source={require("../../assets/images/home.jpg")}
        style={styles.backgroundImage}
      />
      <options.context.Provider
        value={[options.get, options.set]}
      >
        {options.get ? (
          <View
            style={styles.image}
            onTouchStart={(evt) => {
              placecircle(evt.nativeEvent);
            }}
          >
            {type === "camera" ? (
              <CameraComponent context={step === 1 ? "primary" : "secondary"} />
            ) : (
              <ImagePickerComponent />
            )}
            {circle && (
              <Circle
                size={circle.size}
                Xcoord={circle.Xcoord}
                Ycoord={circle.Ycoord}
              />
            )}
          </View>
        ) : (
          <View style={styles.image}>
            {type === "camera" ? (
              <CameraComponent context={step === 1 ? "primary" : "secondary"} />
            ) : (
              <ImagePickerComponent />
            )}
          </View>
        )}

        {options.get ? (
          <View style={styles.buttonContainer}>
            <StyledButton
              type="primary"
              content={"Crop Image"}
              onPress={cropImage}
            />
            <StyledButton
              type="secondary"
              content={"Try Again"}
              onPress={restart}
            />
            {isCropped && (
              <StyledButton
                type="primary"
                content={"Save and Continue"}
                onPress={confirm}
              />
            )}
          </View>
        ) : (
          <View style={styles.textContainer}>
            <CustomText content={"Take a Picture"} style={styles.text} />
          </View>
        )}
      </options.context.Provider>
    </View>
  );
};

export default ImageScreen;

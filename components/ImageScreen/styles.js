import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  image: {
    marginTop: "15%",
    width: "100%",
    alignItems: "center",
  },
  backgroundImage: {
    width: "100%",
    height: "100%",
    resizeMode: "cover",
    position: "absolute",
  },
  buttonsContainer: {
    width: "100%",
    position: "absolute",
  },
  textContainer: {
    flex: 1,
    top: 30,
    width: "100%",
    alignItems: "center",
  },
  text: {
    fontSize: 40,
    fontWeight: "500",
    color: "white",
  },
});

export default styles;

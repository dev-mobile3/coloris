import { StyleSheet, Dimensions } from 'react-native';

const styles = StyleSheet.create({
  
  elementContainer: {
    height: Dimensions.get('window').height*0.2,
    width: Dimensions.get('window').width*0.95,
    //backgroundColor: "#999",
    marginTop: 10,
    flexDirection: 'row',
    borderWidth: 5, 
    borderColor: "black", 
    borderRadius: 30,
    backgroundColor: "#99a"
  },
  centeredContainer: {
    alignItems: 'center', 
    justifyContent: 'center',
  },
  popup: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
    borderColor: "black",
    borderWidth: 4
  },
  bigText: {
    fontSize: 20,
    fontWeight: "bold"
  },
  textInput: {
    borderColor: "black", 
    borderWidth: 2, 
    borderRadius: 5, 
    paddingHorizontal: 10
  }

});

export default styles;
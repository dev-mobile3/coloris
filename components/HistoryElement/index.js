import { View, Text, Image, TouchableOpacity, TextInput, Modal } from 'react-native'
import React, { useState } from 'react'
import styles from './styles'
import ColorCircle from '../ColorCircle';
import { backgroundColor } from 'react-native/Libraries/Components/View/ReactNativeStyleAttributes';
import { ListContext } from '../../Contexts';

const HistoryElement = (props) => {

  //const { name, colors, match } = props;
  const [popupVisible, setPopupVisible] = useState(false);
  const [name, setName] = useState(props.comparison.name);

  const { list, testFunc, updateName } = React.useContext(ListContext);

  //console.log("PROPS : " + props.comparison.name + props.comparison.colors + props.comparison.match);
  
  return (
    
    <View style={styles.elementContainer}>

      <Modal visible={popupVisible} transparent={true}>
        <View style={{...styles.centeredContainer, flex: 1}}>
          <View style={styles.popup}>
            <Text style={styles.bigText}>Entrez un nouveau titre :</Text>
            <TextInput style={styles.textInput}
              defaultValue={props.comparison.name} 
              onChangeText={newText => setName(newText)}
              onSubmitEditing={() => {
                console.log(name);
                list[props.comparison.key].name = name;
                //updateName(props.comparison.key, text);
                setPopupVisible(false);
                //list.forEach(obj => console.log(JSON.stringify(obj)));

              }}
            />
          </View>
        </View>
      </Modal>
      
      {/*Composant*/}
      <View style={{width: '100%', height: '100%'}}>
          
        {/*Barre titre*/}
        <View style={{flex: 1, flexDirection: 'row'}}>
          
          {/*Bouton edit*/}
          <View style={{...styles.centeredContainer, flex: 1}}>
            <TouchableOpacity
              style={{height: '100%', aspectRatio: 1, justifyContent: 'center', alignItems: 'center'}}
              onPress={() => {console.log("bouton : " + props.comparison.name); setPopupVisible(!popupVisible);}}
            >
              <Image source={require('../../assets/images/pencil.png')}/>
            </TouchableOpacity>
          </View>

          {/*Titre*/}
          <View style={{...styles.centeredContainer, flex: 4}}>
            <Text style={{...styles.bigText, fontSize: 20}}>{props.comparison.name}</Text>
          </View>

          {/*Bouton supprimer*/}
          <View style={{...styles.centeredContainer, flex: 1}}>
            <TouchableOpacity
              style={{height: '100%', aspectRatio: 1, justifyContent: 'center', alignItems: 'center'}}
              onPress={() => {
                console.log("supprime : " + props.comparison.name); 
                list.splice(props.comparison.key, 1);
                //to force update :
                //?
              }}
            >
              <Image source={require('../../assets/images/close.png')}/>
            </TouchableOpacity>
          </View>
          
        </View>

        {/*Comparaison*/}
        <View style={{...styles.centeredContainer, justifyContent: 'space-evenly', flex: 2, flexDirection: 'row'}}>

          <ColorCircle color={props.comparison.colors[0]}/>
          <View style={{...styles.centeredContainer, height: '100%', flexGrow: 0.85}}>
            <Text style={styles.bigText}>{props.comparison.match * 100} %</Text>
            <Image source={require('../../assets/images/arrows.png')}/>
          </View>
          <ColorCircle color={props.comparison.colors[1]}/>
        
        </View>

      </View>

      {/*Gros cercle
      <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        <ColorCircle color={"#fff"}/>*/}
        {/*<Image source={require('../../assets/favicon.png')}/>*/}
      {/*</View>*/}
      
      {/*Partie droite
      <View style={{flex: 2}}>
        
        
        
        

      </View>*/}

    </View>
  
  )
}

export default HistoryElement
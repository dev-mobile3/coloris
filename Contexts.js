import React from 'react'

export const ImageContext = React.createContext();
export const ComparisonContext = React.createContext();
export const ListContext = React.createContext();
